Gem::Specification.new do |s|
  s.name        = 'codigo-control'
  s.version     = '0.1.0'
  s.date        = '2019-05-01'
  s.summary     = "Generador de Código de Control"
  s.description = "Generador de Código de Control"
  s.authors     = ["Elmer Mendoza"]
  s.email       = 'defreddyelmer@gmail.com'
  s.files       = Dir.glob("{lib}/**/**/*") + ["Rakefile"]
  s.homepage    = 'https://gitlab.com/la-mapacha/codigo-control'
  s.license     = 'MIT'
end
