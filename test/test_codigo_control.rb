require 'csv'
require 'minitest/autorun'
require 'codigo-control'

class CodigoControlTest < Minitest::Test
  def test_codigo_control
    archivo = '/test/archivos/5000CasosPruebaCCVer7-text.txt'
    nombre_archivo = File.dirname(File.dirname(File.expand_path(__FILE__))) + archivo
    CSV.foreach(nombre_archivo, col_sep: '|', headers: true) do |fila|
      assert_equal fila['CODIGO CONTROL'],
        SIN::CodigoControl.generar(
          fila['NRO. AUTORIZACION'], # Número de autorización
          fila['NRO. FACTURA'], # Número de factura
          fila['NIT/CI'], # Número de cédula de identidad
          fila['FECHA EMISION'].gsub('/', ''), # Fecha de transaccion AAAAMMDD
          fila['MONTO FACTURADO'], # Monto de la transacción
          fila['LLAVE DOSIFICACION'] # Llave de dosificación
        )
    end
  end
end
