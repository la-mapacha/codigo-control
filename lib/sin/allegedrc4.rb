class SIN::AllegedRC4
  def encrypt_message_rca(message, key, unscripted = false)
    state = (0..255).to_a
    x = 0
    y = 0
    index1 = 0
    index2 = 0
    nmen = ''
    message_encryption = ''

    (0..255).to_a.each do |i|
      index2 = (key[index1].ord + state[i] + index2) % 256
      aux = state[i]
      state[i] = state[index2]
      state[index2] = aux
      index1 = (index1 + 1) % key.length
    end

    (0...message.length).to_a.each do |i|
      x = (x + 1) % 256
      y = (state[x] + y) % 256
      aux = state[x]
      state[x] = state[y]
      state[y] = aux
      nmen = message[i].ord ^ state[(state[x] + state[y]) % 256]
      nmen_hex = nmen.to_s(16).upcase
      message_encryption += (unscripted ? '' : '-') + (nmen_hex.length == 1 ? '0' + nmen_hex : nmen_hex)
    end
    unscripted ? message_encryption : message_encryption[1, message_encryption.length]
  end
end
