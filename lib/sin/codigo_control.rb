module SIN
  class CodigoControl
    def self.generar(authorization_number, invoice_number, nitci, date_of_transaction, transaction_amount, dosage_key)
      # Redondeo monto de transacción
      transaction_amount = round_up(transaction_amount).to_s
  
      # Paso 1
      invoice_number = add_verhoeff_digit(invoice_number, 2)
      nitci = add_verhoeff_digit(nitci, 2)
      date_of_transaction = add_verhoeff_digit(date_of_transaction, 2)
      transaction_amount = add_verhoeff_digit(transaction_amount, 2)
      # Se suman todos los valores obtenidos
      sum_of_variables = (invoice_number.to_i +
        nitci.to_i +
        date_of_transaction.to_i +
        transaction_amount.to_i).to_s
      # A la suma total se añade 5 dígitos verhoeff
      sum_of_variables_5_verhoeff = add_verhoeff_digit(sum_of_variables, 5)
  
      # Paso 2
      five_digits_verhoeff = sum_of_variables_5_verhoeff[sum_of_variables_5_verhoeff.length - 5, sum_of_variables_5_verhoeff.length]
      numbers = five_digits_verhoeff.split('').map(&:to_i)
  
      (0...5).each {|i| numbers[i] = numbers[i] + 1}
  
      string1 = dosage_key[0, numbers[0]]
      string2 = dosage_key[numbers[0], numbers[1]]
      string3 = dosage_key[numbers[0] + numbers[1], numbers[2]]
      string4 = dosage_key[numbers[0] + numbers[1] + numbers[2], numbers[3]]
      string5 = dosage_key[numbers[0] + numbers[1] + numbers[2] + numbers[3], numbers[4]]
  
      authorization_number_dkey = authorization_number + string1
      invoice_number_dkey = invoice_number + string2
      nitci_dkey = nitci + string3
      date_of_transaction_dkey = date_of_transaction + string4
      transaction_amount_dkey = transaction_amount + string5
  
      # Paso 3
      string_dkey = authorization_number_dkey + invoice_number_dkey + nitci_dkey + date_of_transaction_dkey + transaction_amount_dkey
      key_for_encryption = dosage_key + five_digits_verhoeff
      allegedrc4_string = AllegedRC4.new.encrypt_message_rca(string_dkey, key_for_encryption, true)
  
      # Paso 4
      chars = allegedrc4_string.split('')
      total_amount = 0
      sp1 = 0
      sp2 = 0
      sp3 = 0
      sp4 = 0
      sp5 = 0
  
      tmp = 1
      (0...allegedrc4_string.length).each do |i|
        total_amount += chars[i].ord
        case tmp
        when 1 then sp1 += chars[i].ord
        when 2 then sp2 += chars[i].ord
        when 3 then sp3 += chars[i].ord
        when 4 then sp4 += chars[i].ord
        when 5 then sp5 += chars[i].ord
        end
        tmp = tmp < 5 ? tmp + 1 : 1
      end
  
      # Paso 5
      tmp1 = total_amount * sp1 / numbers[0]
      tmp2 = total_amount * sp2 / numbers[1]
      tmp3 = total_amount * sp3 / numbers[2]
      tmp4 = total_amount * sp4 / numbers[3]
      tmp5 = total_amount * sp5 / numbers[4]
      sum_product = tmp1 + tmp2 + tmp3 + tmp4 + tmp5
      base64 = Base64.new.convert(sum_product) 
  
      # Paso 6
      AllegedRC4.new.encrypt_message_rca(base64, dosage_key + five_digits_verhoeff)
    end
  
    def self.add_verhoeff_digit(value, max)
      (1..max).each do |i|
        value += Verhoeff.new.calc(value).to_s
      end
      value
    end
  
    def self.round_up(value)
      value2 = value.gsub(',', '.')
      value2.to_f.round
    end
  end
end

require 'sin/verhoeff'
require 'sin/allegedrc4'
require 'sin/base64'
