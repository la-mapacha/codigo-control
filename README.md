# Código de Control

Permite generar los códigos de control para las facturas del Servicio de
Impuestos Nacionales - SIN.

## Instalación

Clonar el repositorio e instalar la gema:

```bash
gem build codigo-control.gemspec
gem install ./codigo-control-0.1.0.gem
```

## Modo de uso

```bash
require 'codigo-control'

codigo = SIN::CodigoControl.generar(
           '7904006306693', # Número de autorización
           '876814',        # Número de factura
           '1665979',       # Número de cédula de identidad
           '20080519',      # Fecha de transaccion AAAAMMDD
           '35958,6',       # Monto de la transacción
           'zZ7Z]xssKqkEf_6K9uH(EcV+%x+u[Cca9T%+_$kiLjT8(zr3T9b5Fx2xG-D+_EBS' # Llave de dosificación
         )

=> "7B-F3-48-A8"
```

## Test

```bash
rake test
```

```bash
Run options: --seed 34409
# Running:
.
Finished in 3.578784s, 0.2794 runs/s, 1397.1227 assertions/s.
1 runs, 5000 assertions, 0 failures, 0 errors, 0 skips
```

## Bibliografía

* https://guides.rubygems.org/make-your-own-gem/
